O projeto foi feito usando a Chili Framework, que j� vem presente com o arquivo.
N�o � necess�rio nenhuma depend�ncia extra.

As classes principais que foram criadas/utilizadas est�o presentes no folder "My Files".
Essas classes s�o: Game, Light, Ray e Sphere (Sphere e Light n�o possuem um cpp).

No Game.cpp s�o posicionados os modelos e � feita a composi��o da cena. 
Os raios s�o mandados de cada pixel na tela na mesma dire��o da camera e � checado se ocorreu alguma intersec��o 
com as esferas. Se ocorrer, � calculado a nova dire��o do raio e a for�a da luz, que depende se houver
oclus�o com outra esfera.

N�o pode ser implementado um filtro no est�gio de p�s-processamento. Modificar e adicionar novos shaders usando a 
framework selecionada se mostrou dificil e sem sucesso.