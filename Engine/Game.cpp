#include "MainWindow.h"
#include "Game.h"
#include "Ray.h"
#include "Mat3.h"
#include "Light.h"

Game::Game( MainWindow& wnd )
	:
	wnd( wnd ),
	gfx( wnd )
{
	spheres = {
		Sphere(Vec3(1e5,0, -2500)  ,1e5,Color(255,0,0)), //Right wall
		Sphere(Vec3(-1e5 ,0, -2500),1e5,Color(0,255,0)), //Left  wall 
		Sphere(Vec3(0,0,1e5 + 100),1e5,Color(0,0,255)),// Back wall
		Sphere(Vec3(0, -1e5, -2500),1e5,Color(200,55,0)),// Bottom wall 
		Sphere(Vec3(0,1e5,2500),1e5,Color(55,200,0)),// Top wall
		Sphere(Vec3(0,0,-1e5 - 100),1e5,Color(0,55,0)),// Front wall

		Sphere(Vec3(0,5,70),8,Color(255,255,255)),// S0
		Sphere(Vec3(-10,-10,90),5,Color(0,255,255)),// S1
		Sphere(Vec3(20,-15,75),13,Color(200,55,200))// S2
	};

	lights = {
		Light(Vec3(20, 25, 25), Colors::White),
		Light(Vec3(20, 25, 75), Colors::White)
	};
}

void Game::Go()
{
	gfx.BeginFrame();
	UpdateModel();
	ComposeFrame();
	gfx.EndFrame();
}

void Game::UpdateModel() {
	//Camera rotation
	const float dt = 1.0f / 60.0f;
	if (wnd.kbd.KeyIsPressed('W')) {
		theta_x = wrap_angle(theta_x - dTheta * dt);
	}
	if (wnd.kbd.KeyIsPressed('A')) {
		theta_y = wrap_angle(theta_y - dTheta * dt);
	}
	if (wnd.kbd.KeyIsPressed('S')) {
		theta_x = wrap_angle(theta_x + dTheta * dt);
	}
	if (wnd.kbd.KeyIsPressed('D')) {
		theta_y = wrap_angle(theta_y + dTheta * dt);
	}

	const Mat3 rot =
		Mat3::RotationX(theta_x)*
		Mat3::RotationY(theta_y);

	//Camera movement
	if (wnd.kbd.KeyIsPressed(VK_UP)) {
		pos += Vec3(0, 0, 1)*rot;
	}
	if (wnd.kbd.KeyIsPressed(VK_DOWN)) {
		pos += Vec3(0, 0, -1)*rot;
	}
	if (wnd.kbd.KeyIsPressed(VK_LEFT)) {
		pos += Vec3(-1, 0, 0)*rot;
	}
	if (wnd.kbd.KeyIsPressed(VK_RIGHT)) {
		pos += Vec3(1, 0, 0)*rot;
	}
}

void Game::ComposeFrame() {
	//Camera position/rotation
	Vec3 camPos = pos;
	const Mat3 rot =
		Mat3::RotationX(theta_x)*
		Mat3::RotationY(theta_y);
	Vec3 viewDir = viewRot*rot;
	float screenDistance = 1.0f;

	//Screen points
	Vec3 screenCenter = camPos + viewDir * screenDistance;
	Vec3 p0 = screenCenter + Vec3(-1, 1, 0)*rot; //Left bottom
	Vec3 p1 = screenCenter + Vec3(1, 1, 0)*rot; //Right bottom
	Vec3 p2 = screenCenter + Vec3(-1, -1, 0)*rot; //Left up

	//Create ray for every pixel
	for (int y = 0; y < Graphics::ScreenHeight; ++y)
	{
		for (int x = 0; x < Graphics::ScreenWidth; ++x)
		{
			//Coords of the pixel 
			float u = (float)x / Graphics::ScreenWidth;
			float v = (float)y / Graphics::ScreenHeight;
			//Get vector of the point
			Vec3 pointOnScreen = p0 + (p1 - p0) * u + (p2 - p0) * v;
			//Direction of the ray
			Vec3 rayDirection = pointOnScreen - camPos;
			Ray ray = Ray(camPos, rayDirection.GetNormalized(), 10000000.0f);
		
			// Intersect all spheres
			int hitIndex = -1;
			for (int i = 0; i < spheres.size(); ++i)
			{
				if (ray.RaySphereIntersection(spheres[i]) == true)
				{
					//Store the hit index
					hitIndex = i;
				}
			}
			if (hitIndex != -1)
			{
				FColor finalColor = Color(0, 0, 0, 0);
				for (int iLight = 0; iLight < lights.size(); ++iLight)
				{
					//Calculate direction of the light ray
					Vec3 hitPoint = ray.origin + ray.direction*ray.length;
					Vec3 normal = (hitPoint - spheres[hitIndex].position);
					normal.Normalize();
					Vec3 lightRayDirection = (lights[iLight].position - hitPoint);
					float length = lightRayDirection.Len();
					lightRayDirection.Normalize();
					Ray lightRay = Ray(hitPoint, lightRayDirection, length);
					// If any of the spheres occlude the light
					bool occluded = false;
					for (int i = 0; i < spheres.size(); ++i)
					{
						if (lightRay.RaySphereIntersection(spheres[i]) == true)
						{
							occluded = true;
							break;
						}
					}
					//If didn't occlude, calculate strenght of the light
					if (occluded == false)
					{
						float d = Dot(normal, lightRayDirection);
						//If it's negative, it won't light (change to 0)
						if (d < 0.0f)
						{
							d = 0.0f;
						}
						finalColor += lights[iLight].color*spheres[hitIndex].color*d;
					}
				}
				finalColor.Clamp();
				gfx.PutPixel(x, y, finalColor);
			}
		}
	}
}