#pragma once

#include "Graphics.h"
#include "Sphere.h"
#include "Light.h"

class Game
{
public:
	Game( class MainWindow& wnd );
	Game( const Game& ) = delete;
	Game& operator=( const Game& ) = delete;
	void Go();

private:
	// Functions
	void ComposeFrame();
	void UpdateModel();

	//Variables
	MainWindow& wnd;
	Graphics gfx;

	//Variables for camera pos/rot
	static constexpr float dTheta = PI;
	float theta_x = 0.0f, theta_y = 0.0f;
	//Vec3 pos = { 10, 10, 20 };
	//Vec3 viewRot = { -1, -1, 1 };
	//Vec3 pos = { 0, 0, 175 };
	//Vec3 viewRot = { 0, 0, -1 };
	Vec3 pos = { 0, 0, 0 };
	Vec3 viewRot = { 0, 0, 1 };


	//Spheres and lights
	std::vector<Sphere> spheres;
	std::vector<Light> lights;
};

